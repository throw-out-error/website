FROM node:alpine

RUN apk add --no-cache bash

# Setting working directory. All the path will be relative to WORKDIR
WORKDIR /usr/src/app

# Copying source files
COPY . .

# Expose the port for the next.js server
EXPOSE 3000

# Building and running the app
CMD [ "/bin/bash", "yarn install && yarn run build && yarn run start" ]
