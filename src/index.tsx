import { h, render } from "preact";
import { Router, Route } from "preact-router";
import { Home } from "./pages/home";
import { Provider as StoreProvider } from "unistore/preact";
import { container } from "./inversify.config";
import { Provider } from "inversify-react";
import AboutPage from "./pages/about";
import Resources from "./pages/resources";
import { Dashboard } from "./pages/dashboard";
import { Login } from "./pages/dashboard/login";
import { Register } from "./pages/dashboard/register";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.scss";
import App from "./pages";
import Layout from "./components/Layout";
import { errorStore } from "./utils";

const dom = document.getElementById("root");
render(
    <Provider container={container}>
        <StoreProvider store={errorStore}>
            <Router>
                <Layout>
                    <Route path="/" component={App} exact />
                    <Route path="/home" component={Home} exact />
                    <Route path="/about" component={AboutPage} exact />
                    <Route path="/resources" component={Resources} exact />
                    <Route path="/dashboard" component={Dashboard} exact />
                    <Route path="/dashboard/login" component={Login} exact />
                    <Route
                        path="/dashboard/register"
                        component={Register}
                        exact
                    />
                </Layout>
            </Router>
        </StoreProvider>
    </Provider>,
    dom!
);
