import { injectable } from "inversify";
import { apiUrl, Errors, sanitizeData } from "src/utils";
import { get, set } from "../session";
import axios, { AxiosResponse } from "axios";
import { User } from "src/typings";

export type Response = Promise<{
    response?: AxiosResponse;
    error?: Error | string;
    json?: never;
}>;

@injectable()
export class AuthService {
    isLoggedIn(): boolean {
        return get("token") !== undefined && get("user") !== undefined;
    }

    hasRole(role: string): boolean {
        return this.isLoggedIn()
            ? (get<User>("user") as User).roles.includes(role)
            : false;
    }

    async logout(): Response {
        try {
            const response = await axios.post(
                apiUrl + "/auth/logout",
                { token: get<string>("token") },
                {
                    headers: {
                        Accept: "application/json",
                    },
                },
            );
            set([
                ["user", null],
                ["token", null],
            ]);
            return { response };
        } catch (error) {
            set([
                ["user", null],
                ["token", null],
            ]);
            return { error };
        }
    }

    async deletePost(data: { title: string }): Response {
        try {
            const result = await axios.post(
                apiUrl + "/blog/post",
                JSON.stringify(sanitizeData(data)),
                {
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                },
            );
            if (!result) return { error: Errors.NO_RESPONSE };
            const json = result.data;
            if (result.status === 200) {
                return {
                    response: result,
                    json,
                };
            } else if (result.status === 401) {
                return {
                    error: Errors.AUTHENTICATION,
                    response: result,
                    json,
                };
            } else {
                return {
                    error: "Unknown error encountered.",
                    response: result,
                    json,
                };
            }
        } catch (e) {
            console.error(e);
            return {
                error: e.message,
            };
        }
    }

    async createPost(data: {
        title: string;
        description: string;
        content: string;
    }): Response {
        try {
            const result = await axios.post(
                apiUrl + "/blog/post",
                JSON.stringify(sanitizeData(data)),
                {
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                },
            );
            if (!result) return { error: Errors.NO_RESPONSE };
            const json = result.data;
            if (result.status === 201) {
                return {
                    response: result,
                    json,
                };
            } else if (result.status === 401) {
                return {
                    error: Errors.AUTHENTICATION,
                    response: result,
                    json,
                };
            } else {
                return {
                    error: json.message ?? json.error,
                    response: result,
                    json,
                };
            }
        } catch (e) {
            console.error(e);
            return {
                error: e.message,
            };
        }
    }

    async getProfile(accessToken?: string): Response {
        try {
            const result = await axios.get(apiUrl + "/auth/profile", {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: `${accessToken}`,
                },
            });
            if (!result) return { error: Errors.NO_RESPONSE };

            const json = result.data;

            if (result.status === 200) {
                set([["user", json.user]]);
                return {
                    response: result,
                    json,
                };
            } else {
                set([
                    ["user", null],
                    ["token", null],
                ]);
                return {
                    error: Errors.AUTHENTICATION,
                    response: result,
                    json,
                };
            }
        } catch (e) {
            return {
                error: Errors.AUTHENTICATION,
            };
        }
    }

    async sendLoginRequest(username: string, password: string): Response {
        let result: AxiosResponse | undefined;
        try {
            result = await axios.post(
                apiUrl + "/auth/user",
                {
                    username: username.toLowerCase(),
                    password,
                },
                {
                    method: "POST",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                },
            );
            if (!result) return { error: Errors.NO_RESPONSE };
            const res = result.data;

            if (result.status === 200 && res.accessToken) {
                set([["token", res.accessToken]]);
                // Get profile if logged in
                return await this.getProfile(res.accessToken);
            } else {
                set([
                    ["user", null],
                    ["token", null],
                ]);
                return {
                    error: Errors.AUTHENTICATION,
                };
            }
        } catch (e) {
            set([
                ["user", null],
                ["token", null],
            ]);
            return {
                error:
                    e.response.data.error ??
                    e.response.statusText ??
                    "Unknown Error",
            };
        }
    }

    async sendRegistrationRequest(data: {
        username: string;
        password: string;
        name?: string;
    }): Response {
        try {
            const result = await axios.post(
                apiUrl + "/auth/user/create",
                { ...data, username: data.username.toLowerCase() },
                {
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                    method: "POST",
                },
            );
            const json = await result.data;
            if (result.status === 200) {
                set([["user", json.user]]);
                return { response: result, json };
            } else {
                set([["user", null]]);
                return { error: Errors.AUTHENTICATION, response: result, json };
            }
        } catch (e) {
            console.error(e);
            return { error: e.message };
        }
    }

    getProjects(): Promise<unknown[]> {
        return new Promise((resolve, reject) => {
            fetch(apiUrl + "/projects")
                .then((res) => res.json())
                .then(resolve)
                .catch(reject);
        });
    }

    getPosts(opts: {
        dateFilter?: string;
        category?: string;
        query?: string;
    }): Promise<unknown[]> {
        const formattedUrl = `${apiUrl}/blog/posts?date=${
            opts.dateFilter ?? ""
        }&category=${opts.category ?? ""}&search=${opts.query ?? ""}`;

        return new Promise((resolve, reject) => {
            console.log(formattedUrl);
            fetch(formattedUrl)
                .then((res) => res.json())
                // Convert date string to js Date class
                .then((posts) =>
                    posts.map((p: any) => ({
                        ...p,
                        createdAt: new Date(p.createdAt),
                    })),
                )
                .then(resolve)
                .catch(reject);
        });
    }

    dateToString(dateObj: Date): string {
        const monthNames = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
        const month = monthNames[dateObj.getMonth()];
        const day = String(dateObj.getDate()).padStart(2, "0");
        const year = dateObj.getFullYear();
        const output = month + "\n" + day + "," + year;
        return output;
    }

    // TODO: add interface types for post and user
    getSinglePost(id: string): Promise<unknown[]> {
        return new Promise((resolve, reject) => {
            fetch(`${apiUrl}/blog/post/${id}`)
                .then((res) => res.json())
                .then((post) => ({
                    ...post,
                    createdAt: new Date(post.createdAt),
                }))
                .then(resolve)
                .catch(reject);
        });
    }
}
