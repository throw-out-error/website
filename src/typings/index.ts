export type User = {
    username: string;
    name: string;
    roles: string[];
    avatar?: string;
};
