export function set(data: Array<[string, never | undefined | null]>): void {
    data.forEach((d) => {
        if (d[1]) {
            sessionStorage.setItem(
                d[0],
                (typeof d[1] === "string"
                    ? d[1]
                    : JSON.stringify(d[1])) as string,
            );
        } else sessionStorage.removeItem(d[0]);
    });
}

export function get<T>(key: string): T | undefined {
    const item = sessionStorage.getItem(key);
    try {
        if (item) return JSON.parse(item);
        else return;
    } catch {
        if (item) return (item as unknown) as T;
        else return;
    }
}
