import React, { ReactNode, useState, useEffect } from "react";
import { Container } from "react-bootstrap";

import { Footer } from "./Footer";
import { Header } from "./Header";
import { Helmet } from "react-helmet";
import { useHistory } from "react-router-dom";
import ErrorDialog from "./ErrorDialog";

type Props = {
    children?: ReactNode;
    title?: string;
};

const Layout = ({ children, title = "Throw Out Error" }: Props) => {
    const history = useHistory();
    const [currentPage, setCurrentPage] = useState("Index");
    useEffect(() => {
        const words = history.location.pathname
            .split("")
            .splice(1)
            .join("")
            .replace("/", " ")
            .split(" ");
        for (let i = 0; i < words.length; i++)
            words[i] = words[i].charAt(0).toUpperCase() + words[i].substr(1);
        setCurrentPage(words.join(" "));
        document.title = currentPage;
    }, [history, currentPage]);
    return (
        <div>
            <Helmet>
                <title>{`${currentPage} | ${title}`}</title>
                <meta charSet="utf-8" />
                <meta
                    name="viewport"
                    content="initial-scale=1.0, width=device-width"
                />
            </Helmet>

            <div id="page-container">
                <Header />
                <Container id="content">
                    {children}
                    <ErrorDialog />
                </Container>

                <Footer />
            </div>
        </div>
    );
};

export default Layout;
