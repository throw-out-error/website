import React, { Component } from "react";
import { Row, Toast } from "react-bootstrap";
import { Unsubscribe } from "redux";
import { errorStore } from "../utils";

export default class ErrorDialog extends Component {
    timeout?: NodeJS.Timeout;
    state: { display: boolean; error: string };
    errorStoreSubscription?: Unsubscribe;

    constructor(props: never) {
        super(props);
        this.state = {
            display: false,
            error: "",
        };
        this.toggleError = this.toggleError.bind(this);
        this.getError = this.getError.bind(this);
    }

    componentDidMount(): void {
        this.errorStoreSubscription = errorStore.subscribe(() => {
            const error = errorStore.getState();
            this.setState({
                error,
                display: error !== undefined && error !== "",
            });
            // Hide error dialog after n milliseconds
            this.timeout = setTimeout(() => {
                this.toggleError();
            }, 10000);
        });
    }

    componentWillUnmount(): void {
        if (this.errorStoreSubscription) this.errorStoreSubscription();
    }

    toggleError(): void {
        if (this.timeout) clearTimeout(this.timeout);
        if (this.state.display) this.setState({ display: false });
        else this.setState({ display: true });
    }

    getError(): string {
        return this.state.error;
    }

    render() {
        return (
            <Row className="flex-column align-items-center">
                <Toast
                    style={{
                        backgroundColor: "#555",
                        marginBottom: this.state.display ? "25px" : 0,
                        marginTop: this.state.display ? "25px" : 0,
                        transition: "opacity 0.5s linear, height 0.5s linear",
                        display: this.state.display ? "flex" : "none",
                        flexDirection: "column",
                        minWidth: `${250}px`,
                        height: this.state.display
                            ? `${this.state.error.length + 25}px`
                            : 0,
                        flexBasis: 0,
                    }}
                    onClose={this.toggleError}
                >
                    <Toast.Header className="w-100">
                        <strong className="mr-auto">Error</strong>
                    </Toast.Header>
                    <Toast.Body>{this.getError()}</Toast.Body>
                </Toast>
            </Row>
        );
    }
}
