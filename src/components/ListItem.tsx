import { h } from "preact";
import { Link } from "react-router-dom";

import { Project } from "../utils/data";

type Props = {
    data: Project;
};

const ListItem = ({ data }: Props) => (
    <Link to={`/projects/${data.id}`}>
        <a>{data.name}</a>
    </Link>
);

export default ListItem;
