import React, { FunctionComponentElement } from "react";

export const Footer = (): FunctionComponentElement<HTMLDivElement> => (
    <footer>
        <small id="toes-are-epic">Toes are epic.</small>
        <p>
            {" "}
            Designed By{" "}
            <a
                href="https://theoparis.com/about"
                target="_blank"
                rel="noopener noreferrer"
            >
                Theo Paris
            </a>{" "}
            & Powered By
            <a
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
            >
                {" "}
                React
            </a>
            .
        </p>
    </footer>
);
