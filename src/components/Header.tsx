import { h } from "preact";
import { Col, Nav, Navbar, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

export const Header = () => (
    <Navbar className="header flex-column" expand="lg">
        <Col>
            <Row className="flex-column align-items-center">
                <Link to="/home">
                    <img
                        className="logo"
                        src="/images/logo-transparent.svg"
                        alt="throw out error()"
                    ></img>
                </Link>
            </Row>
            <Row className="flex-column align-items-center">
                <Navbar.Toggle
                    className="text-center"
                    aria-controls="navbarContent"
                >
                    <span className="navbar-toggler-icon"></span>
                </Navbar.Toggle>
            </Row>
            <Row className="flex-column align-items-center">
                <Navbar.Collapse id="navbarContent">
                    <Nav className="header-nav">
                        <li key="about" className="nav-item">
                            <Link to="/about" className="nav-link">
                                About Us
                            </Link>
                        </li>
                        <li key="projects" className="nav-item">
                            <Link to="/projects" className="nav-link">
                                Projects
                            </Link>
                        </li>
                        <li key="resources" className="nav-item">
                            <Link to="/resources" className="nav-link">
                                Resources
                            </Link>
                        </li>
                        <li key="dashboard" className="nav-item">
                            <Link to="/dashboard" className="nav-link">
                                Dashboard
                            </Link>
                        </li>
                    </Nav>
                </Navbar.Collapse>
            </Row>
        </Col>
    </Navbar>
);
