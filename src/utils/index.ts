import { sanitize } from "dompurify";
import createStore from "unistore";

export const apiUrl = process.env.REACT_APP_API || "http://localhost:8080";

export const sanitizeData = (data: any) =>
    Object.values(data).map((v) => (typeof v === "string" ? sanitize(v) : v));

function error(state = "", action: { type: string; error: string }) {
    if (action.type === "set") state = action.error;
    return state;
}

export const errorStore = createStore(error);

export enum Errors {
    AUTHENTICATION = "Authentication error encountered - double check your credentials and make sure you are logged in.",
    NO_RESPONSE = "No response from backend.",
}
