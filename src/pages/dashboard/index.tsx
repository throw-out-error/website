import React, { Component, FormEvent, CSSProperties } from "react";
import { Link } from "react-router-dom";
import { get } from "../../session";
import { Form } from "react-bootstrap";
import { resolve } from "inversify-react";
import { AuthService } from "src/services/auth.service";
import { User } from "src/typings";
import { errorStore } from "src/utils";

const mbStyle: CSSProperties = {
    display: "block",
    marginBottom: "15px",
};

export class Dashboard extends Component<any, { user?: User }> {
    @resolve(AuthService)
    private authService!: AuthService;

    constructor(props: any) {
        super(props);
        this.state = {};
    }

    componentDidMount(): void {
        // Fetch latest profile information from backend
        if (get("token"))
            this.authService.getProfile(get("token")).then((result) => {
                if (!result) return;

                if (!result.error) this.props.history.push("/dashboard");
                else {
                    errorStore.dispatch({
                        type: "set",
                        error: result.error.toString(),
                    });
                }
                if (!result.json) return;
                this.setState({
                    user: ((result.json as unknown) as { user?: User }).user,
                });
            });
    }

    logOut(event: FormEvent): void {
        event.preventDefault();
        this.authService
            .logout()
            .then(() => this.props.history.push("/dashboard"));
    }

    render() {
        return (
            <div>
                <h2>Dashboard</h2>
                {this.authService.isLoggedIn() ? (
                    <div className="dashboard">
                        <h3 style={mbStyle}>
                            Welcome,{" "}
                            {(get("user") as User).name ||
                                (get("user") as User).username}
                            .
                        </h3>
                        {this.authService.hasRole("admin") ? (
                            <a style={mbStyle} href="/admin">
                                Admin Dashboard
                            </a>
                        ) : (
                            <div />
                        )}
                        {this.authService.hasRole("writer") ? (
                            <a style={mbStyle} href="/admin/blog">
                                Blog Administration Dashboard
                            </a>
                        ) : (
                            <div />
                        )}
                        <Form onSubmit={this.logOut.bind(this)}>
                            <Form.Group>
                                <button
                                    className="form-control btn btn-primary"
                                    type="submit"
                                >
                                    Log Out
                                </button>
                            </Form.Group>
                        </Form>
                    </div>
                ) : (
                    <div>
                        <Link to="/dashboard/login">Login</Link>
                        <br />
                        <Link to="/dashboard/register">Register</Link>
                        <br />
                    </div>
                )}
            </div>
        );
    }
}
