import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";

export default function App() {
    const router = useHistory();

    useEffect(() => {
        setTimeout(() => router.push("/home"), 5000);
    }, [router]);
    return (
        <h3 className="error">
            Syntax error on 69:420. Please wait to be redirected...
        </h3>
    );
}
